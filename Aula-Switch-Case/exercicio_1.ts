namespace exercicio_1 {
  let nivel: string;
  nivel = "basico";

  switch (nivel) {
    case "avançado":
      console.log("Parabéns seu nível é avançado! ");
      break;
    case "intermediario":
      console.log("Parabéns seu nível é intermediario! ");
      break;
    case "basico":
      console.log("Parabéns seu nível é básico! ");
      break;
    default:
      console.log("Não é possivel reconhecer esse nível!");
  }
}
