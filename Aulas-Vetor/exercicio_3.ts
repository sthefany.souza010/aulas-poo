namespace exercicio_3 {
    let livros: any[] = [
    {titulo: "Titulo 1", autor:"Autor1"},
    {titulo: "Titulo 2", autor:"Autor2"},
    {titulo: "Titulo 3", autor:"Autor3"},
    {titulo: "Titulo 4", autor:"Autor4"},
    {titulo: "Titulo 5", autor:"Autor5"},
    ];
    let autores = livros.map((livro)=>{
        return livro.titulo
    });
    let titulos = livros.map((livro)=>{
        return livro.titulo
    });

    console.log(autores);
    console.log(titulos);

    let LivrosAutor3 = livros.filter((livro)=>{
        return livro.autor === "Autor 3"
    })
    console.log(LivrosAutor3);

    let titulosAutor3 = LivrosAutor3.map((livro)=>{
        return livro.titulo;
    })
    console.log(titulosAutor3);
}